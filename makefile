DOC=dokument
PDF=xdg-open
TEX=pdfcsplain

$(DOC).pdf: $(DOC).tex dot
	vlna $(DOC).tex
	$(TEX) $(DOC).tex
	$(TEX) $(DOC).tex

clean:
	rm $(DOC).log $(DOC).pdf graph.pdf -f

run: $(DOC).pdf
	$(PDF) $(DOC).pdf

refresh: $(DOC).pdf
	pkill -SIGHUP mupdf

upload:
	scp $(DOC).pdf martinfranc.eu:/usr/share/nginx/homepage/tex.pdf

dot: graph.dot
	dot -Tpdf graph.dot -o graph.pdf