#!/usr/bin/python3
from flask import Flask, send_file, request
import tempfile
from subprocess import Popen as popen
from urllib.parse import urlparse

app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
	eq = urlparse(request.url).query
	with tempfile.NamedTemporaryFile('w+', encoding='utf-8') as fp:
		print('\\hsize=16383.99999pt', file=fp)
		print('\\nopagenumbers', file=fp)
		print('$${}$$\\end'.format(eq), file=fp)
		fp.flush()

		popen(['tex', '-halt-on-error', fp.name], 
			cwd=tempfile.tempdir).wait()
		dvi = fp.name + '.dvi'
		
		popen(['dvipng', '-T', 'tight', fp.name + '.dvi'], 
			cwd=tempfile.tempdir).wait()
		img = fp.name + '1.png'

		return send_file(img, mimetype='image/png')
		

if __name__ == "__main__":
	app.run()