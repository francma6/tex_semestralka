# ketex_speed.hml
Testovací rychlostní skript pro KeTeX. Využívá HTML5 storage pro uložení mezivýsledků. Pro spuštění testů stačí otevřít stránku v prohlížeči.

# mathjax_speed
Testovací rychlostní skript pro MathJax. Použití jako u KeTeXu.
